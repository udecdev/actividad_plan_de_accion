/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='images/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "both",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'stage1',
                            type: 'group',
                            rect: ['-10', '-9', '1043', '670', 'auto', 'auto'],
                            c: [
                            {
                                id: 'Imagen-Slide1',
                                type: 'image',
                                rect: ['0px', '9px', '1043px', '661px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Imagen-Slide1.png",'0px','0px']
                            },
                            {
                                id: 'Fondo-Slide1',
                                type: 'image',
                                rect: ['410px', '0px', '624px', '658px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Fondo-Slide1.png",'0px','0px']
                            },
                            {
                                id: 'Grafico-Slide1',
                                type: 'image',
                                rect: ['587px', '209px', '96px', '97px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Grafico-Slide1.png",'0px','0px']
                            },
                            {
                                id: 'Titulo-Slide1',
                                type: 'image',
                                rect: ['583px', '329px', '283px', '193px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Titulo-Slide1.png",'0px','0px']
                            }]
                        },
                        {
                            id: 'stage2',
                            type: 'group',
                            rect: ['0px', '0px', '1024', '640', 'auto', 'auto'],
                            c: [
                            {
                                id: 'slider',
                                type: 'group',
                                rect: ['0', '0', '1024', '640', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'slider1',
                                    display: 'block',
                                    type: 'group',
                                    rect: ['0px', '0px', '1024px', '640px', 'auto', 'auto'],
                                    userClass: "slider",
                                    c: [
                                    {
                                        id: 't2',
                                        type: 'text',
                                        rect: ['120px', '258px', '198px', '70px', 'auto', 'auto'],
                                        text: "<p style=\"margin: 0px;\">​Instrucciones</p>",
                                        align: "right",
                                        font: ['Arial, Helvetica, sans-serif', [26, "px"], "rgba(0,0,0,1)", "normal", "none", "", "break-word", "normal"]
                                    },
                                    {
                                        id: 't3',
                                        type: 'text',
                                        rect: ['348px', '435px', '498px', '70px', 'auto', 'auto'],
                                        text: "<p style=\"margin: 0px; text-align: left;\">​Recuerde que únicamente posee 2 intentos</p><p style=\"margin: 0px;\">​</p><p style=\"margin: 0px; text-align: left;\"></p>",
                                        align: "right",
                                        font: ['Arial, Helvetica, sans-serif', [21, "px"], "rgba(0,168,156,1.00)", "normal", "none", "", "break-word", "normal"]
                                    },
                                    {
                                        id: 't1',
                                        type: 'text',
                                        rect: ['350px', '130px', '425px', '70px', 'auto', 'auto'],
                                        text: "<p style=\"margin: 0px; text-align: left;\">​<span style=\"font-size: 18px;\">Usted es jefe de una dependencia y ha realizado un autodiagnóstico de factores internos y externos que están incidiendo directa e indirectamente en el área de trabajo, según las funciones administrativas del liderazgo, desarrolladas por usted.</span></p><p style=\"margin: 0px;\"><span style=\"font-size: 18px;\">​</span></p><p style=\"margin: 0px; text-align: left;\"><span style=\"font-size: 18px;\">De acuerdo con el siguiente listado de factores, le sugerimos que por favor los clasifique en fortalezas, debilidades, oportunidades y amenazas, en la casilla correspondiente.</span></p><p style=\"margin: 0px;\">​</p>",
                                        align: "right",
                                        font: ['Arial, Helvetica, sans-serif', [26, "px"], "rgba(0,0,0,1)", "normal", "none", "", "break-word", "normal"]
                                    },
                                    {
                                        id: 'Separador-Slide2',
                                        type: 'image',
                                        rect: ['329px', '135px', '2px', '330px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"Separador-Slide2.png",'0px','0px']
                                    },
                                    {
                                        id: 'Grafico_1-Slide2',
                                        type: 'image',
                                        rect: ['263px', '200px', '55px', '56px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"Grafico_1-Slide2.png",'0px','0px']
                                    }]
                                },
                                {
                                    id: 'slider2',
                                    display: 'block',
                                    type: 'group',
                                    rect: ['61', '52', '913', '495', 'auto', 'auto'],
                                    userClass: "slider",
                                    c: [
                                    {
                                        id: 'table1',
                                        type: 'image',
                                        rect: ['364px', '8px', '549px', '474px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"table1.svg",'0px','0px']
                                    },
                                    {
                                        id: 'op20',
                                        type: 'group',
                                        rect: ['125px', '450px', '116', '45', 'auto', 'auto'],
                                        userClass: "dragg1",
                                        c: [
                                        {
                                            id: 'Op20-Slide3',
                                            type: 'image',
                                            rect: ['0px', '0px', '116px', '45px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Op20-Slide3.png",'0px','0px']
                                        },
                                        {
                                            id: 'Op20-Slide3a',
                                            type: 'image',
                                            rect: ['0px', '0px', '116px', '44px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Op20-Slide3a.png",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'op19',
                                        type: 'group',
                                        rect: ['125px', '400px', '117', '45', 'auto', 'auto'],
                                        userClass: "dragg1",
                                        c: [
                                        {
                                            id: 'Op19-Slide3',
                                            type: 'image',
                                            rect: ['0px', '0px', '117px', '45px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Op19-Slide3.png",'0px','0px']
                                        },
                                        {
                                            id: 'Op19-Slide3a',
                                            type: 'image',
                                            rect: ['0px', '0px', '116px', '44px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Op19-Slide3a.png",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'op18',
                                        type: 'group',
                                        rect: ['125px', '350px', '116', '45', 'auto', 'auto'],
                                        userClass: "dragg1",
                                        c: [
                                        {
                                            id: 'Op18-Slide3',
                                            type: 'image',
                                            rect: ['0px', '0px', '116px', '45px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Op18-Slide3.png",'0px','0px']
                                        },
                                        {
                                            id: 'Op18-Slide3a',
                                            type: 'image',
                                            rect: ['0px', '0px', '116px', '44px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Op18-Slide3a.png",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'op17',
                                        type: 'group',
                                        rect: ['125px', '300px', '116', '45', 'auto', 'auto'],
                                        userClass: "dragg1",
                                        c: [
                                        {
                                            id: 'Op17-Slide3',
                                            type: 'image',
                                            rect: ['0px', '0px', '116px', '45px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Op17-Slide3.png",'0px','0px']
                                        },
                                        {
                                            id: 'Op17a-Slide3',
                                            type: 'image',
                                            rect: ['0px', '0px', '116px', '44px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Op17a-Slide3.png",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'op16',
                                        type: 'group',
                                        rect: ['125px', '250px', '116', '45', 'auto', 'auto'],
                                        userClass: "dragg1",
                                        c: [
                                        {
                                            id: 'Op16-Slide3',
                                            type: 'image',
                                            rect: ['0px', '0px', '116px', '45px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Op16-Slide3.png",'0px','0px']
                                        },
                                        {
                                            id: 'Op16a-Slide3',
                                            type: 'image',
                                            rect: ['0px', '0px', '116px', '44px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Op16a-Slide3.png",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'op15',
                                        type: 'group',
                                        rect: ['125px', '200px', '116', '45', 'auto', 'auto'],
                                        userClass: "dragg1",
                                        c: [
                                        {
                                            id: 'Op15-Slide3',
                                            type: 'image',
                                            rect: ['0px', '0px', '116px', '45px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Op15-Slide3.png",'0px','0px']
                                        },
                                        {
                                            id: 'Op15a-Slide3',
                                            type: 'image',
                                            rect: ['0px', '0px', '116px', '44px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Op15a-Slide3.png",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'op14',
                                        type: 'group',
                                        rect: ['125px', '150px', '117', '45', 'auto', 'auto'],
                                        userClass: "dragg1",
                                        c: [
                                        {
                                            id: 'Op14-Slide3',
                                            type: 'image',
                                            rect: ['0px', '0px', '117px', '45px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Op14-Slide3.png",'0px','0px']
                                        },
                                        {
                                            id: 'Op14a-Slide3',
                                            type: 'image',
                                            rect: ['0px', '0px', '116px', '44px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Op14a-Slide3.png",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'op13',
                                        type: 'group',
                                        rect: ['125px', '100px', '117', '45', 'auto', 'auto'],
                                        userClass: "dragg1",
                                        c: [
                                        {
                                            id: 'Op13-Slide3',
                                            type: 'image',
                                            rect: ['0px', '0px', '117px', '45px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Op13-Slide3.png",'0px','0px']
                                        },
                                        {
                                            id: 'Op13a-Slide3',
                                            type: 'image',
                                            rect: ['0px', '0px', '116px', '44px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Op13a-Slide3.png",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'op12',
                                        type: 'group',
                                        rect: ['125px', '50px', '117', '45', 'auto', 'auto'],
                                        userClass: "dragg1",
                                        c: [
                                        {
                                            id: 'Op12-Slide3',
                                            type: 'image',
                                            rect: ['0px', '0px', '117px', '45px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Op12-Slide3.png",'0px','0px']
                                        },
                                        {
                                            id: 'Op12a-Slide3',
                                            type: 'image',
                                            rect: ['0px', '0px', '116px', '44px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Op12a-Slide3.png",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'op11',
                                        type: 'group',
                                        rect: ['125px', '0px', '116', '45', 'auto', 'auto'],
                                        userClass: "dragg1",
                                        c: [
                                        {
                                            id: 'Op11-Slide3',
                                            type: 'image',
                                            rect: ['0px', '0px', '116px', '45px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Op11-Slide3.png",'0px','0px']
                                        },
                                        {
                                            id: 'Op11a-Slide3',
                                            type: 'image',
                                            rect: ['0px', '0px', '116px', '44px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Op11a-Slide3.png",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'op10',
                                        type: 'group',
                                        rect: ['0px', '450px', '116', '45', 'auto', 'auto'],
                                        userClass: "dragg1",
                                        c: [
                                        {
                                            id: 'Op10-Slide3',
                                            type: 'image',
                                            rect: ['0px', '0px', '116px', '45px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Op10-Slide3.png",'0px','0px']
                                        },
                                        {
                                            id: 'Op10a-Slide3',
                                            type: 'image',
                                            rect: ['0px', '0px', '116px', '44px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Op10a-Slide3.png",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'op9',
                                        type: 'group',
                                        rect: ['0px', '400px', '117', '45', 'auto', 'auto'],
                                        userClass: "dragg1",
                                        c: [
                                        {
                                            id: 'Op9-Slide3',
                                            type: 'image',
                                            rect: ['0px', '0px', '117px', '45px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Op9-Slide3.png",'0px','0px']
                                        },
                                        {
                                            id: 'Op9a-Slide3',
                                            type: 'image',
                                            rect: ['0px', '0px', '116px', '44px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Op9a-Slide3.png",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'op8',
                                        type: 'group',
                                        rect: ['0px', '350px', '117', '45', 'auto', 'auto'],
                                        userClass: "dragg1",
                                        c: [
                                        {
                                            id: 'Op8-Slide3',
                                            type: 'image',
                                            rect: ['0px', '0px', '117px', '45px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Op8-Slide3.png",'0px','0px']
                                        },
                                        {
                                            id: 'Op8a-Slide3',
                                            type: 'image',
                                            rect: ['0px', '0px', '116px', '44px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Op8a-Slide3.png",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'op7',
                                        type: 'group',
                                        rect: ['0px', '300px', '117', '45', 'auto', 'auto'],
                                        userClass: "dragg1",
                                        c: [
                                        {
                                            id: 'Op7-Slide3',
                                            type: 'image',
                                            rect: ['0px', '0px', '117px', '45px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Op7-Slide3.png",'0px','0px']
                                        },
                                        {
                                            id: 'Op7a-Slide3',
                                            type: 'image',
                                            rect: ['0px', '0px', '116px', '44px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Op7a-Slide3.png",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'op6',
                                        type: 'group',
                                        rect: ['0px', '250px', '117', '45', 'auto', 'auto'],
                                        userClass: "dragg1",
                                        c: [
                                        {
                                            id: 'Op6-Slide3',
                                            type: 'image',
                                            rect: ['0px', '0px', '117px', '45px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Op6-Slide3.png",'0px','0px']
                                        },
                                        {
                                            id: 'Op6a-Slide3',
                                            type: 'image',
                                            rect: ['0px', '0px', '116px', '44px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Op6a-Slide3.png",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'op5',
                                        type: 'group',
                                        rect: ['0px', '200px', '116', '45', 'auto', 'auto'],
                                        userClass: "dragg1",
                                        c: [
                                        {
                                            id: 'Op5-Slide3',
                                            type: 'image',
                                            rect: ['0px', '0px', '116px', '45px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Op5-Slide3.png",'0px','0px']
                                        },
                                        {
                                            id: 'Op5a-Slide3',
                                            type: 'image',
                                            rect: ['0px', '0px', '116px', '44px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Op5a-Slide3.png",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'op4',
                                        type: 'group',
                                        rect: ['0px', '150px', '116', '45', 'auto', 'auto'],
                                        userClass: "dragg1",
                                        c: [
                                        {
                                            id: 'Op4-Slide3',
                                            type: 'image',
                                            rect: ['0px', '0px', '116px', '45px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Op4-Slide3.png",'0px','0px']
                                        },
                                        {
                                            id: 'Op4a-Slide3',
                                            type: 'image',
                                            rect: ['0px', '0px', '116px', '44px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Op4a-Slide3.png",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'op3',
                                        type: 'group',
                                        rect: ['0px', '100px', '117', '45', 'auto', 'auto'],
                                        userClass: "dragg1",
                                        c: [
                                        {
                                            id: 'Op3-Slide3',
                                            type: 'image',
                                            rect: ['0px', '0px', '117px', '45px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Op3-Slide3.png",'0px','0px']
                                        },
                                        {
                                            id: 'Op3a-Slide3',
                                            type: 'image',
                                            rect: ['0px', '0px', '116px', '44px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Op3a-Slide3.png",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'op2',
                                        type: 'group',
                                        rect: ['0px', '50px', '117', '45', 'auto', 'auto'],
                                        userClass: "dragg1",
                                        c: [
                                        {
                                            id: 'Op2-Slide3',
                                            type: 'image',
                                            rect: ['0px', '0px', '117px', '45px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Op2-Slide3.png",'0px','0px']
                                        },
                                        {
                                            id: 'Op2a-Slide3',
                                            type: 'image',
                                            rect: ['0px', '0px', '116px', '44px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Op2a-Slide3.png",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'op1',
                                        type: 'group',
                                        rect: ['0px', '0px', '116', '45', 'auto', 'auto'],
                                        userClass: "dragg1",
                                        c: [
                                        {
                                            id: 'Op1-Slide3',
                                            type: 'image',
                                            rect: ['0px', '0px', '116px', '45px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Op1-Slide3.png",'0px','0px']
                                        },
                                        {
                                            id: 'Op1a-Slide3',
                                            type: 'image',
                                            rect: ['0px', '0px', '116px', '44px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Op1a-Slide3.png",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'r-1-1',
                                        type: 'rect',
                                        rect: ['410px', '106px', '117px', '45px', 'auto', 'auto'],
                                        fill: ["rgba(192,192,192,0)"],
                                        stroke: [0,"rgb(0, 0, 0)","none"],
                                        userClass: "dropp1"
                                    },
                                    {
                                        id: 'r-1-2',
                                        type: 'rect',
                                        rect: ['410px', '152px', '117px', '45px', 'auto', 'auto'],
                                        fill: ["rgba(192,192,192,0)"],
                                        stroke: [0,"rgb(0, 0, 0)","none"],
                                        userClass: "dropp1"
                                    },
                                    {
                                        id: 'r-1-3',
                                        type: 'rect',
                                        rect: ['410px', '200px', '117px', '45px', 'auto', 'auto'],
                                        fill: ["rgba(192,192,192,0)"],
                                        stroke: [0,"rgb(0, 0, 0)","none"],
                                        userClass: "dropp1"
                                    },
                                    {
                                        id: 'r-1-4',
                                        type: 'rect',
                                        rect: ['531px', '106px', '117px', '45px', 'auto', 'auto'],
                                        fill: ["rgba(192,192,192,0)"],
                                        stroke: [0,"rgb(0, 0, 0)","none"],
                                        userClass: "dropp1"
                                    },
                                    {
                                        id: 'r-1-5',
                                        type: 'rect',
                                        rect: ['531px', '152px', '117px', '45px', 'auto', 'auto'],
                                        fill: ["rgba(192,192,192,0)"],
                                        stroke: [0,"rgb(0, 0, 0)","none"],
                                        userClass: "dropp1"
                                    },
                                    {
                                        id: 'r-2-1',
                                        type: 'rect',
                                        rect: ['673px', '106px', '117px', '45px', 'auto', 'auto'],
                                        fill: ["rgba(192,192,192,0)"],
                                        stroke: [0,"rgb(0, 0, 0)","none"],
                                        userClass: "dropp1"
                                    },
                                    {
                                        id: 'r-2-2',
                                        type: 'rect',
                                        rect: ['673px', '152px', '117px', '45px', 'auto', 'auto'],
                                        fill: ["rgba(192,192,192,0)"],
                                        stroke: [0,"rgb(0, 0, 0)","none"],
                                        userClass: "dropp1"
                                    },
                                    {
                                        id: 'r-2-3',
                                        type: 'rect',
                                        rect: ['673px', '200px', '117px', '45px', 'auto', 'auto'],
                                        fill: ["rgba(192,192,192,0)"],
                                        stroke: [0,"rgb(0, 0, 0)","none"],
                                        userClass: "dropp1"
                                    },
                                    {
                                        id: 'r-2-4',
                                        type: 'rect',
                                        rect: ['794px', '106px', '117px', '45px', 'auto', 'auto'],
                                        fill: ["rgba(192,192,192,0)"],
                                        stroke: [0,"rgb(0, 0, 0)","none"],
                                        userClass: "dropp1"
                                    },
                                    {
                                        id: 'r-2-5',
                                        type: 'rect',
                                        rect: ['794px', '152px', '117px', '45px', 'auto', 'auto'],
                                        fill: ["rgba(192,192,192,0)"],
                                        stroke: [0,"rgb(0, 0, 0)","none"],
                                        userClass: "dropp1"
                                    },
                                    {
                                        id: 'r-3-1',
                                        type: 'rect',
                                        rect: ['410px', '313px', '117px', '45px', 'auto', 'auto'],
                                        fill: ["rgba(192,192,192,0)"],
                                        stroke: [0,"rgb(0, 0, 0)","none"],
                                        userClass: "dropp1"
                                    },
                                    {
                                        id: 'r-3-2',
                                        type: 'rect',
                                        rect: ['410px', '359px', '117px', '45px', 'auto', 'auto'],
                                        fill: ["rgba(192,192,192,0)"],
                                        stroke: [0,"rgb(0, 0, 0)","none"],
                                        userClass: "dropp1"
                                    },
                                    {
                                        id: 'r-3-3',
                                        type: 'rect',
                                        rect: ['410px', '407px', '117px', '45px', 'auto', 'auto'],
                                        fill: ["rgba(192,192,192,0)"],
                                        stroke: [0,"rgb(0, 0, 0)","none"],
                                        userClass: "dropp1"
                                    },
                                    {
                                        id: 'r-3-4',
                                        type: 'rect',
                                        rect: ['531px', '313px', '117px', '45px', 'auto', 'auto'],
                                        fill: ["rgba(192,192,192,0)"],
                                        stroke: [0,"rgb(0, 0, 0)","none"],
                                        userClass: "dropp1"
                                    },
                                    {
                                        id: 'r-3-5',
                                        type: 'rect',
                                        rect: ['531px', '359px', '117px', '45px', 'auto', 'auto'],
                                        fill: ["rgba(192,192,192,0)"],
                                        stroke: [0,"rgb(0, 0, 0)","none"],
                                        userClass: "dropp1"
                                    },
                                    {
                                        id: 'r-4-1',
                                        type: 'rect',
                                        rect: ['673px', '313px', '117px', '45px', 'auto', 'auto'],
                                        fill: ["rgba(192,192,192,0)"],
                                        stroke: [0,"rgb(0, 0, 0)","none"],
                                        userClass: "dropp1"
                                    },
                                    {
                                        id: 'r-4-2',
                                        type: 'rect',
                                        rect: ['673px', '359px', '117px', '45px', 'auto', 'auto'],
                                        fill: ["rgba(192,192,192,0)"],
                                        stroke: [0,"rgb(0, 0, 0)","none"],
                                        userClass: "dropp1"
                                    },
                                    {
                                        id: 'r-4-3',
                                        type: 'rect',
                                        rect: ['673px', '407px', '117px', '45px', 'auto', 'auto'],
                                        fill: ["rgba(192,192,192,0)"],
                                        stroke: [0,"rgb(0, 0, 0)","none"],
                                        userClass: "dropp1"
                                    },
                                    {
                                        id: 'r-4-4',
                                        type: 'rect',
                                        rect: ['794px', '313px', '117px', '45px', 'auto', 'auto'],
                                        fill: ["rgba(192,192,192,0)"],
                                        stroke: [0,"rgb(0, 0, 0)","none"],
                                        userClass: "dropp1"
                                    },
                                    {
                                        id: 'r-4-5',
                                        type: 'rect',
                                        rect: ['794px', '359px', '117px', '45px', 'auto', 'auto'],
                                        fill: ["rgba(192,192,192,0)"],
                                        stroke: [0,"rgb(0, 0, 0)","none"],
                                        userClass: "dropp1"
                                    }]
                                },
                                {
                                    id: 'slider3',
                                    display: 'block',
                                    type: 'group',
                                    rect: ['0px', '0px', '1024px', '640px', 'auto', 'auto'],
                                    userClass: "slider",
                                    c: [
                                    {
                                        id: 'dragg-1',
                                        type: 'group',
                                        rect: ['474px', '345px', '116', '44', 'auto', 'auto'],
                                        userClass: "dragg2",
                                        c: [
                                        {
                                            id: 'o1',
                                            type: 'image',
                                            rect: ['0px', '0px', '116px', '44px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"o1.svg",'0px','0px']
                                        },
                                        {
                                            id: 'o11',
                                            type: 'image',
                                            rect: ['0px', '0px', '116px', '44px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"o11.svg",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'dragg-2',
                                        type: 'group',
                                        rect: ['474px', '445px', '116', '44', 'auto', 'auto'],
                                        userClass: "dragg2",
                                        c: [
                                        {
                                            id: 'o2',
                                            type: 'image',
                                            rect: ['0px', '0px', '116px', '44px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"o2.svg",'0px','0px']
                                        },
                                        {
                                            id: 'o22',
                                            type: 'image',
                                            rect: ['0px', '0px', '116px', '44px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"o22.svg",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'dragg-3',
                                        type: 'group',
                                        rect: ['474px', '395px', '116', '44', 'auto', 'auto'],
                                        userClass: "dragg2",
                                        c: [
                                        {
                                            id: 'o3',
                                            type: 'image',
                                            rect: ['0px', '0px', '116px', '44px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"o3.svg",'0px','0px']
                                        },
                                        {
                                            id: 'o33',
                                            type: 'image',
                                            rect: ['0px', '0px', '116px', '44px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"o33.svg",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'dragg-4',
                                        type: 'group',
                                        rect: ['596px', '445px', '346', '44', 'auto', 'auto'],
                                        userClass: "dragg2",
                                        c: [
                                        {
                                            id: 'o4',
                                            type: 'image',
                                            rect: ['0px', '0px', '346px', '44px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"o4.svg",'0px','0px']
                                        },
                                        {
                                            id: 'o44',
                                            type: 'image',
                                            rect: ['0px', '0px', '345px', '44px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"o44.svg",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'dragg-5',
                                        type: 'group',
                                        rect: ['596px', '396px', '346', '44', 'auto', 'auto'],
                                        userClass: "dragg2",
                                        c: [
                                        {
                                            id: 'o5',
                                            type: 'image',
                                            rect: ['0px', '0px', '346px', '44px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"o5.svg",'0px','0px']
                                        },
                                        {
                                            id: 'o55',
                                            type: 'image',
                                            rect: ['0px', '0px', '345px', '44px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"o55.svg",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'dragg-6',
                                        type: 'group',
                                        rect: ['596px', '345px', '346', '44', 'auto', 'auto'],
                                        userClass: "dragg2",
                                        c: [
                                        {
                                            id: 'o6',
                                            type: 'image',
                                            rect: ['0px', '0px', '346px', '44px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"o6.svg",'0px','0px']
                                        },
                                        {
                                            id: 'o66',
                                            type: 'image',
                                            rect: ['0px', '0px', '345px', '44px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"o66.svg",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'r-1',
                                        type: 'rect',
                                        rect: ['473px', '163px', '116px', '44px', 'auto', 'auto'],
                                        fill: ["rgba(192,192,192,0)"],
                                        stroke: [0,"rgb(0, 0, 0)","none"],
                                        userClass: "dropp2"
                                    },
                                    {
                                        id: 'r-2',
                                        type: 'rect',
                                        rect: ['473px', '213px', '116px', '44px', 'auto', 'auto'],
                                        fill: ["rgba(192,192,192,0)"],
                                        stroke: [0,"rgb(0, 0, 0)","none"],
                                        userClass: "dropp2"
                                    },
                                    {
                                        id: 'r-3',
                                        type: 'rect',
                                        rect: ['473px', '263px', '116px', '44px', 'auto', 'auto'],
                                        fill: ["rgba(192,192,192,0)"],
                                        stroke: [0,"rgb(0, 0, 0)","none"],
                                        userClass: "dropp2"
                                    },
                                    {
                                        id: 'r-4',
                                        type: 'rect',
                                        rect: ['597px', '163px', '345px', '44px', 'auto', 'auto'],
                                        fill: ["rgba(192,192,192,0)"],
                                        stroke: [0,"rgb(0, 0, 0)","none"],
                                        userClass: "dropp2"
                                    },
                                    {
                                        id: 'r-5',
                                        type: 'rect',
                                        rect: ['597px', '213px', '345px', '44px', 'auto', 'auto'],
                                        fill: ["rgba(192,192,192,0)"],
                                        stroke: [0,"rgb(0, 0, 0)","none"],
                                        userClass: "dropp2"
                                    },
                                    {
                                        id: 'r-6',
                                        type: 'rect',
                                        rect: ['597px', '263px', '345px', '44px', 'auto', 'auto'],
                                        fill: ["rgba(192,192,192,0)"],
                                        stroke: [0,"rgb(0, 0, 0)","none"],
                                        userClass: "dropp2"
                                    },
                                    {
                                        id: 'table2',
                                        type: 'image',
                                        rect: ['473px', '113px', '470px', '218px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"table2.svg",'0px','0px']
                                    },
                                    {
                                        id: 't3-1',
                                        type: 'text',
                                        rect: ['99px', '222px', '317px', '189px', 'auto', 'auto'],
                                        text: "<p style=\"margin: 0px; text-align: left;\"><span style=\"font-size: 18px;\">Según los resultados del autodiagnóstico realizado, le presentamos a continuación, 2 columnas, una con opciones de funciones de liderazgo a implementar y otra con opciones de definición de cada función a implementar. Usted seleccionará en cada caso la opción correcta y las relacionará en el siguiente cuadro​</span></p>",
                                        align: "right",
                                        font: ['Arial, Helvetica, sans-serif', [21, "px"], "rgba(0,168,156,1)", "400", "none", "normal", "break-word", "normal"],
                                        textStyle: ["", "", "", "", "none"]
                                    },
                                    {
                                        id: 'Grafico_2-Slide4',
                                        type: 'image',
                                        rect: ['101px', '132px', '83px', '84px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"Grafico_2-Slide4.png",'0px','0px']
                                    }]
                                },
                                {
                                    id: 'slider4',
                                    type: 'rect',
                                    rect: ['0px', '0px', '1024px', '640px', 'auto', 'auto'],
                                    fill: ["rgba(192,192,192,0)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "slider",
                                    c: [
                                    {
                                        id: 'Grafico_3-Slide5',
                                        type: 'image',
                                        rect: ['97px', '125px', '169px', '169px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"Grafico_3-Slide5.png",'0px','0px']
                                    },
                                    {
                                        id: 't4-1',
                                        type: 'text',
                                        rect: ['312px', '125px', '570px', '101px', 'auto', 'auto'],
                                        text: "<p style=\"margin: 0px; text-align: left;\">​Ahora, le invitamos a identificar el objetivo, meta, actividades, recursos y responsables asociados a cada una de las funciones de liderazgo que le reseñamos a continuación. Le recomendamos que lea muy bien cada enunciado, a fin de arrastrarlo en la casilla correspondiente, teniendo presente que las actividades deben estar organizadas según su secuencia lógica, para la implementación de la función administrativa de liderazgo&nbsp;</p><p style=\"margin: 0px;\">​</p>",
                                        align: "right",
                                        font: ['Arial, Helvetica, sans-serif', [21, "px"], "rgba(0,168,156,1)", "400", "none", "normal", "break-word", "normal"],
                                        textStyle: ["", "", "", "", "none"]
                                    },
                                    {
                                        id: 't4-2',
                                        type: 'text',
                                        rect: ['312px', '345px', '570px', '85px', 'auto', 'auto'],
                                        text: "<p style=\"margin: 0px; text-align: left;\">​Recuerde que posee únicamente dos intentos para desarrollar esta actividad</p><p style=\"margin: 0px; text-align: left;\">​</p>",
                                        align: "right",
                                        font: ['Arial, Helvetica, sans-serif', [21, "px"], "rgba(0,168,156,1)", "400", "none", "normal", "break-word", "normal"],
                                        textStyle: ["", "", "", "", "none"]
                                    },
                                    {
                                        id: 't4-3',
                                        type: 'text',
                                        rect: ['312px', '416px', '570px', '31px', 'auto', 'auto'],
                                        text: "<p style=\"margin: 0px; text-align: left;\">​¡Muchos éxitos!</p><p style=\"margin: 0px;\">​</p><p style=\"margin: 0px; text-align: left;\">​</p>",
                                        align: "right",
                                        font: ['Arial, Helvetica, sans-serif', [21, "px"], "rgba(0,168,156,1)", "400", "none", "normal", "break-word", "normal"],
                                        textStyle: ["", "", "", "", "none"]
                                    }]
                                },
                                {
                                    id: 'slider5',
                                    type: 'group',
                                    rect: ['31', '37', '806', '531', 'auto', 'auto'],
                                    userClass: "slider",
                                    c: [
                                    {
                                        id: 'slide-Slide6',
                                        type: 'image',
                                        rect: ['381px', '0px', '425px', '531px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"slide-Slide6.png",'0px','0px']
                                    },
                                    {
                                        id: 'dr-12',
                                        type: 'group',
                                        rect: ['145px', '335px', '136', '62', 'auto', 'auto'],
                                        userClass: "dragg3",
                                        c: [
                                        {
                                            id: 'op12_1-Slide6',
                                            type: 'image',
                                            rect: ['0px', '0px', '136px', '62px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"op12_1-Slide6.png",'0px','0px']
                                        },
                                        {
                                            id: 'op12-Slide6',
                                            type: 'image',
                                            rect: ['0px', '0px', '136px', '61px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"op12-Slide6.png",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'dr-11',
                                        type: 'group',
                                        rect: ['145px', '268px', '136', '62', 'auto', 'auto'],
                                        userClass: "dragg3",
                                        c: [
                                        {
                                            id: 'op11_1-Slide6',
                                            type: 'image',
                                            rect: ['0px', '0px', '136px', '62px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"op11_1-Slide6.png",'0px','0px']
                                        },
                                        {
                                            id: 'op11-Slide6',
                                            type: 'image',
                                            rect: ['0px', '0px', '136px', '61px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"op11-Slide6.png",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'dr-10',
                                        type: 'group',
                                        rect: ['145px', '201px', '136', '62', 'auto', 'auto'],
                                        userClass: "dragg3",
                                        c: [
                                        {
                                            id: 'op10_1-Slide6',
                                            type: 'image',
                                            rect: ['0px', '0px', '136px', '62px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"op10_1-Slide6.png",'0px','0px']
                                        },
                                        {
                                            id: 'op10-Slide6',
                                            type: 'image',
                                            rect: ['0px', '0px', '136px', '61px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"op10-Slide6.png",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'dr-9',
                                        type: 'group',
                                        rect: ['145px', '134px', '136', '62', 'auto', 'auto'],
                                        userClass: "dragg3",
                                        c: [
                                        {
                                            id: 'op9_1-Slide6',
                                            type: 'image',
                                            rect: ['0px', '0px', '136px', '62px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"op9_1-Slide6.png",'0px','0px']
                                        },
                                        {
                                            id: 'op9-Slide6',
                                            type: 'image',
                                            rect: ['0px', '0px', '136px', '61px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"op9-Slide6.png",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'dr-8',
                                        type: 'group',
                                        rect: ['145px', '67px', '136', '62', 'auto', 'auto'],
                                        userClass: "dragg3",
                                        c: [
                                        {
                                            id: 'op8_1-Slide6',
                                            type: 'image',
                                            rect: ['0px', '0px', '136px', '62px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"op8_1-Slide6.png",'0px','0px']
                                        },
                                        {
                                            id: 'op8-Slide6',
                                            type: 'image',
                                            rect: ['0px', '0px', '136px', '62px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"op8-Slide6.png",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'dr-7',
                                        type: 'group',
                                        rect: ['145px', '0px', '136', '62', 'auto', 'auto'],
                                        userClass: "dragg3",
                                        c: [
                                        {
                                            id: 'op7_1-Slide6',
                                            type: 'image',
                                            rect: ['0px', '0px', '136px', '62px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"op7_1-Slide6.png",'0px','0px']
                                        },
                                        {
                                            id: 'op7-Slide6',
                                            type: 'image',
                                            rect: ['0px', '0px', '136px', '61px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"op7-Slide6.png",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'dr-6',
                                        type: 'group',
                                        rect: ['0px', '335px', '136', '62', 'auto', 'auto'],
                                        userClass: "dragg3",
                                        c: [
                                        {
                                            id: 'op6_2-Slide6',
                                            type: 'image',
                                            rect: ['0px', '0px', '136px', '62px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"op6_2-Slide6.png",'0px','0px']
                                        },
                                        {
                                            id: 'op6_1-Slide6',
                                            type: 'image',
                                            rect: ['0px', '0px', '136px', '61px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"op6_1-Slide6.png",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'dr-5',
                                        type: 'group',
                                        rect: ['0px', '268px', '136', '62', 'auto', 'auto'],
                                        userClass: "dragg3",
                                        c: [
                                        {
                                            id: 'op5_2-Slide6',
                                            type: 'image',
                                            rect: ['0px', '0px', '136px', '62px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"op5_2-Slide6.png",'0px','0px']
                                        },
                                        {
                                            id: 'op5_1-Slide6',
                                            type: 'image',
                                            rect: ['0px', '0px', '136px', '62px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"op5_1-Slide6.png",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'dr-4',
                                        type: 'group',
                                        rect: ['0px', '201px', '136', '62', 'auto', 'auto'],
                                        userClass: "dragg3",
                                        c: [
                                        {
                                            id: 'op4_2-Slide6',
                                            type: 'image',
                                            rect: ['0px', '0px', '136px', '62px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"op4_2-Slide6.png",'0px','0px']
                                        },
                                        {
                                            id: 'op4_1-Slide6',
                                            type: 'image',
                                            rect: ['0px', '0px', '136px', '61px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"op4_1-Slide6.png",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'dr-3',
                                        type: 'group',
                                        rect: ['0px', '134px', '136', '62', 'auto', 'auto'],
                                        userClass: "dragg3",
                                        c: [
                                        {
                                            id: 'op3_2-Slide6',
                                            type: 'image',
                                            rect: ['0px', '0px', '136px', '62px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"op3_2-Slide6.png",'0px','0px']
                                        },
                                        {
                                            id: 'op3_1-Slide6',
                                            type: 'image',
                                            rect: ['0px', '0px', '136px', '61px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"op3_1-Slide6.png",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'dr-2',
                                        type: 'group',
                                        rect: ['0px', '67px', '136', '62', 'auto', 'auto'],
                                        userClass: "dragg3",
                                        c: [
                                        {
                                            id: 'op2_2-Slide6',
                                            type: 'image',
                                            rect: ['0px', '0px', '136px', '62px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"op2_2-Slide6.png",'0px','0px']
                                        },
                                        {
                                            id: 'op2_1-Slide6',
                                            type: 'image',
                                            rect: ['0px', '0px', '136px', '61px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"op2_1-Slide6.png",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'dr-1',
                                        type: 'group',
                                        rect: ['0px', '0px', '136', '62', 'auto', 'auto'],
                                        userClass: "dragg3",
                                        c: [
                                        {
                                            id: 'op1_2-Slide6',
                                            type: 'image',
                                            rect: ['0px', '0px', '136px', '62px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"op1_2-Slide6.png",'0px','0px']
                                        },
                                        {
                                            id: 'op1_1-Slide6',
                                            type: 'image',
                                            rect: ['0px', '0px', '136px', '61px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"op1_1-Slide6.png",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'rec-1',
                                        type: 'rect',
                                        rect: ['382px', '35px', '136px', '62px', 'auto', 'auto'],
                                        fill: ["rgba(192,192,192,0)"],
                                        stroke: [0,"rgb(0, 0, 0)","none"],
                                        userClass: "dropp3"
                                    },
                                    {
                                        id: 'rec-2',
                                        type: 'rect',
                                        rect: ['526px', '35px', '136px', '62px', 'auto', 'auto'],
                                        fill: ["rgba(192,192,192,0)"],
                                        stroke: [0,"rgb(0, 0, 0)","none"],
                                        userClass: "dropp3"
                                    },
                                    {
                                        id: 'rec-3',
                                        type: 'rect',
                                        rect: ['670px', '35px', '136px', '62px', 'auto', 'auto'],
                                        fill: ["rgba(192,192,192,0)"],
                                        stroke: [0,"rgb(0, 0, 0)","none"],
                                        userClass: "dropp3"
                                    },
                                    {
                                        id: 'rec-4',
                                        type: 'rect',
                                        rect: ['382px', '136px', '136px', '62px', 'auto', 'auto'],
                                        fill: ["rgba(192,192,192,0)"],
                                        stroke: [0,"rgb(0, 0, 0)","none"],
                                        userClass: "dropp3"
                                    },
                                    {
                                        id: 'rec-5',
                                        type: 'rect',
                                        rect: ['526px', '136px', '136px', '62px', 'auto', 'auto'],
                                        fill: ["rgba(192,192,192,0)"],
                                        stroke: [0,"rgb(0, 0, 0)","none"],
                                        userClass: "dropp3"
                                    },
                                    {
                                        id: 'rec-6',
                                        type: 'rect',
                                        rect: ['670px', '136px', '136px', '62px', 'auto', 'auto'],
                                        fill: ["rgba(192,192,192,0)"],
                                        stroke: [0,"rgb(0, 0, 0)","none"],
                                        userClass: "dropp3"
                                    },
                                    {
                                        id: 'rec-7',
                                        type: 'rect',
                                        rect: ['382px', '203px', '136px', '62px', 'auto', 'auto'],
                                        fill: ["rgba(192,192,192,0)"],
                                        stroke: [0,"rgb(0, 0, 0)","none"],
                                        userClass: "dropp3"
                                    },
                                    {
                                        id: 'rec-8',
                                        type: 'rect',
                                        rect: ['526px', '203px', '136px', '62px', 'auto', 'auto'],
                                        fill: ["rgba(192,192,192,0)"],
                                        stroke: [0,"rgb(0, 0, 0)","none"],
                                        userClass: "dropp3"
                                    },
                                    {
                                        id: 'rec-9',
                                        type: 'rect',
                                        rect: ['382px', '270px', '136px', '62px', 'auto', 'auto'],
                                        fill: ["rgba(192,192,192,0)"],
                                        stroke: [0,"rgb(0, 0, 0)","none"],
                                        userClass: "dropp3"
                                    },
                                    {
                                        id: 'rec-10',
                                        type: 'rect',
                                        rect: ['382px', '336px', '136px', '62px', 'auto', 'auto'],
                                        fill: ["rgba(192,192,192,0)"],
                                        stroke: [0,"rgb(0, 0, 0)","none"],
                                        userClass: "dropp3"
                                    },
                                    {
                                        id: 'rec-11',
                                        type: 'rect',
                                        rect: ['382px', '402px', '136px', '62px', 'auto', 'auto'],
                                        fill: ["rgba(192,192,192,0)"],
                                        stroke: [0,"rgb(0, 0, 0)","none"],
                                        userClass: "dropp3"
                                    },
                                    {
                                        id: 'rec-12',
                                        type: 'rect',
                                        rect: ['382px', '469px', '136px', '62px', 'auto', 'auto'],
                                        fill: ["rgba(192,192,192,0)"],
                                        stroke: [0,"rgb(0, 0, 0)","none"],
                                        userClass: "dropp3"
                                    }]
                                },
                                {
                                    id: 'slider6',
                                    type: 'rect',
                                    rect: ['0px', '0px', '1024px', '641px', 'auto', 'auto'],
                                    fill: ["rgba(192,192,192,0)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "slider",
                                    c: [
                                    {
                                        id: 'Rectangle6',
                                        type: 'rect',
                                        rect: ['1px', '-1px', '493px', '643px', 'auto', 'auto'],
                                        fill: ["rgba(124,190,44,1.00)"],
                                        stroke: [0,"rgb(0, 0, 0)","none"]
                                    },
                                    {
                                        id: 't6-1',
                                        type: 'text',
                                        rect: ['40px', '176px', '410px', '129px', 'auto', 'auto'],
                                        text: "<p style=\"margin: 0px;\">​¡Hemos realizado la planeación de la implementación de las funciones del liderazgo!</p><p style=\"margin: 0px;\">​</p>",
                                        align: "right",
                                        font: ['Arial, Helvetica, sans-serif', [21, "px"], "rgba(0,168,156,1)", "400", "none", "normal", "break-word", "normal"],
                                        textStyle: ["", "", "", "", "none"]
                                    },
                                    {
                                        id: 't6-2',
                                        type: 'text',
                                        rect: ['534px', '157px', '410px', '230px', 'auto', 'auto'],
                                        text: "<p style=\"margin: 0px;\"><span style=\"font-family: Arial, Helvetica, sans-serif; font-weight: 400; font-style: normal; text-decoration: none; font-size: 21px; color: rgb(0, 168, 156); background-color: rgba(0, 0, 0, 0); letter-spacing: 0px; text-transform: none; word-spacing: 0px;\">Esperamos que estas actividades le hayan permitido definir con claridad la forma como se debe implementar las funciones del liderazgo en el área de trabajo, según el estilo de gerencia adoptado.</span></p><p style=\"margin: 0px;\"><span style=\"font-family: Arial, Helvetica, sans-serif; font-weight: 400; font-style: normal; text-decoration: none; font-size: 21px; color: rgb(0, 168, 156); background-color: rgba(0, 0, 0, 0); letter-spacing: 0px; text-transform: none; word-spacing: 0px;\">​</span></p><p style=\"margin: 0px;\"><span style=\"font-family: Arial, Helvetica, sans-serif; font-weight: 400; font-style: normal; text-decoration: none; font-size: 21px; color: rgb(0, 168, 156); background-color: rgba(0, 0, 0, 0); letter-spacing: 0px; text-transform: none; word-spacing: 0px;\">​</span>Le invitamos a continuación, para que socialice su experiencia personal y laboral, con respecto a las funciones del liderazgo en su área de trabajo</p><p style=\"margin: 0px;\"><span style=\"font-family: Arial, Helvetica, sans-serif; font-weight: 400; font-style: normal; text-decoration: none; font-size: 21px; color: rgb(0, 168, 156); background-color: rgba(0, 0, 0, 0); letter-spacing: 0px; text-transform: none; word-spacing: 0px;\"></span>&nbsp;</p><p style=\"margin: 0px;\">​</p>",
                                        align: "left",
                                        font: ['Arial, Helvetica, sans-serif', [21, "px"], "rgba(0,168,156,1)", "400", "none", "normal", "break-word", "normal"],
                                        textStyle: ["", "", "", "", "none"]
                                    }]
                                }]
                            },
                            {
                                id: 'btninicio',
                                type: 'image',
                                rect: ['477px', '582px', '104px', '43px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"btninicio.png",'0px','0px']
                            },
                            {
                                id: 'btn_atr',
                                type: 'image',
                                rect: ['366px', '582px', '105px', '43px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"btnatrashover.png",'0px','0px']
                            },
                            {
                                id: 'btn_sig',
                                type: 'image',
                                rect: ['587px', '582px', '104px', '42px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"btnsiguientehover.png",'0px','0px']
                            }]
                        },
                        {
                            id: 'preload',
                            type: 'group',
                            rect: ['0', '0', '1024', '640', 'auto', 'auto'],
                            c: [
                            {
                                id: 'fondo-preload',
                                type: 'rect',
                                rect: ['0px', '0px', '1024px', '640px', 'auto', 'auto'],
                                fill: ["rgba(255,255,255,1)"],
                                stroke: [0,"rgb(0, 0, 0)","none"]
                            },
                            {
                                id: 'logo',
                                type: 'image',
                                rect: ['462px', '270px', '100px', '100px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"logo.svg",'0px','0px']
                            }]
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '1024px', '640px', 'auto', 'auto'],
                            overflow: 'hidden',
                            fill: ["rgba(255,255,255,1)"]
                        }
                    }
                },
                timeline: {
                    duration: 0,
                    autoPlay: true,
                    data: [
                        [
                            "eid37",
                            "display",
                            0,
                            0,
                            "linear",
                            "${slider3}",
                            'block',
                            'block'
                        ],
                        [
                            "eid40",
                            "left",
                            0,
                            0,
                            "linear",
                            "${slider3}",
                            '0px',
                            '0px'
                        ],
                        [
                            "eid38",
                            "width",
                            0,
                            0,
                            "linear",
                            "${slider3}",
                            '1024px',
                            '1024px'
                        ],
                        [
                            "eid41",
                            "top",
                            0,
                            0,
                            "linear",
                            "${slider3}",
                            '0px',
                            '0px'
                        ],
                        [
                            "eid36",
                            "display",
                            0,
                            0,
                            "linear",
                            "${slider2}",
                            'block',
                            'block'
                        ],
                        [
                            "eid39",
                            "height",
                            0,
                            0,
                            "linear",
                            "${slider3}",
                            '640px',
                            '640px'
                        ],
                        [
                            "eid35",
                            "display",
                            0,
                            0,
                            "linear",
                            "${slider1}",
                            'block',
                            'block'
                        ]
                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

    if (!window.edge_authoring_mode) AdobeEdge.getComposition(compId).load("index_edgeActions.js");
})("EDGE-247297");
