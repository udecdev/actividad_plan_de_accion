var ST = "#Stage_";
var S = "Stage_";
ivo.info({
    title: "Guia General",
    autor: "Edilson Laverde Molina",
    date: "",
    email: "edilsonlaverde_182@hotmail.com",
    icon: 'https://www.ucundinamarca.edu.co/images/iconos/favicon_verde_1.png',
    facebook: "https://www.facebook.com/edilson.laverde.molina"
});
var audios = [
    {
        url: "sonidos/click.mp3",
        name: "clic"
    },{
        url: "sonidos/bad.mp3",
        name: "bad"
    },{
        url: "sonidos/good.mp3",
        name: "good"
    }
];
const relation1 =[
    {
        dropps: ["r-1-1","r-1-2","r-1-3","r-1-4","r-1-5"],
        draggs:["op17","op1","op15","op14","op2"]
    },
    {
        dropps: ["r-2-1","r-2-2","r-2-3","r-2-4","r-2-5"],
        draggs:["op13","op16","op18","op19","op20"]
    },
    {
        dropps: ["r-3-1","r-3-2","r-3-3","r-3-4","r-3-5"],
        draggs:["op4","op9","op10","op11","op12"]
    },
    {
        dropps: ["r-4-1","r-4-2","r-4-3","r-4-4","r-4-5"],
        draggs:["op3","op5","op6","op7","op8"]
    }
];
const relation2 = [
    {
        dropp:"r-1",
        dragg:"dragg-1",
    },
    {
        dropp:"r-2",
        dragg:"dragg-2",
    },
    {
        dropp:"r-3",
        dragg:"dragg-3",
    },
    {
        dropp:"r-4",
        dragg:"dragg-4",
    },
    {
        dropp:"r-5",
        dragg:"dragg-5",
    },
    {
        dropp:"r-6",
        dragg:"dragg-6",
    }
];
const relation3 =[
    {
        dropps: ["rec-1"],
        draggs:["dr-11"]
    },{
        dropps: ["rec-2"],
        draggs:["dr-10"]
    },{
        dropps: ["rec-3"],
        draggs:["dr-9"]
    },{
        dropps: ["rec-5","rec-8"],
        draggs:["dr-7","dr-5"],
    },{
        dropps: ["rec-6"],
        draggs:["dr-6"],
    },{
        dropps: ["rec-4","rec-7","rec-9","rec-10","rec-11","rec-12"],
        draggs:["dr-12","dr-8","dr-4","dr-3","dr-2","dr-1"]
    }
];
let points = 0;
let limit1 = 25;
let limit2 = 6;
let limit3 = 12;
let ejercicios = 43;
let increment =  50/ejercicios ;
function main(sym) {
    var t = null;
    udec = ivo.structure({
        created: function () {
            t = this;
            t.game_dragg_drop1();
            t.game_dragg_drop2();
            t.game_dragg_drop3();
            t.events();
            t.animations();
            //precarga audios//
            ivo.load_audio(audios, onComplete = function () {
                ivo(ST + "preload").hide();
                stage1.play();
            });
        },
        methods: {
            game_dragg_drop1: function () {
                let btns = document.querySelectorAll(".dragg1 div:nth-of-type(2)");
                //los ocultamos
                btns.forEach(function (btn) {
                    btn.style.display = "none";
                });
                $(".dragg1").draggable({
                    revert: "invalid",
                    //centramos el mousel del dragg
                    cursor: "move",
                    cursorAt: {
                        top: 20,
                        left:54
                    },
                    start: function (event, ui) {
                        //ivo.play_audio("clic");
                    }
                });
                $(".dropp1").droppable({
                    accept: ".dragg1",
                    drop: function (event, ui) {
                        let dragg = ui.draggable.attr("id").split("Stage_")[1];
                        let dropp = $(this).attr("id").split("Stage_")[1];
                        let correct = false;
                        var _this = this;
                        
                        let {top, left} = $(this).position();
                        limit1--;
                        console.log($(this).position());
                        relation1.forEach(function (item, index) {
                            let {dropps,draggs} = item;
                            //verrificamos si el dropp esta en los dropps
                            if (dropps.indexOf(dropp) != -1) {
                                //verificamos si el dragg esta en los draggs
                                console.log(ST+dragg);
                                if (draggs.indexOf(dragg) != -1) {
                                    correct = true;
                                    points+=increment;
                                    document.querySelector(ST+dragg+" div:nth-of-type(2)").style.display = "block";
                                    ivo.play("good");
                                    ui.draggable.css({
                                        top:top+"px",
                                        left:left+"px",
                                    });
                                    ui.draggable.css({
                                        cursor:"default"
                                    });
                                    //desactimamos draggable
                                    ui.draggable.draggable("disable");
                                    //desactimamos droppable
                                    $(_this).droppable("disable");
                                }else{
                                    ivo.play("bad");
                                    ivo(ST+dropp).hide();
                                    ui.draggable.css({
                                        top:top+"px",
                                        left:left+"px",
                                    });
                                    ui.draggable.css({
                                        cursor:"default"
                                    });
                                    //desactimamos draggable
                                    ui.draggable.draggable("disable");
                                    //desactimamos droppable
                                    $(_this).droppable("disable");
                                }
                            }
                        });
                        if(limit1==0){
                            //debemos pasar segundo escenario del juego
                            slider.go(3);
                        }
                    }
                });

            },
            game_dragg_drop2: function () {
                let btns = document.querySelectorAll(".dragg2 div:nth-of-type(2)");
                //los ocultamos
                btns.forEach(function (btn) {
                    btn.style.display = "none";
                });
                $(".dragg2").draggable({
                    revert: "invalid",
                    //centramos el mousel del dragg
                    cursor: "move",
                    cursorAt: {
                        top: 20,
                        left:54
                    },
                    start: function (event, ui) {
                        //ivo.play_audio("clic");
                    }
                });
                $(".dropp2").droppable({
                    accept: ".dragg2",
                    drop: function (event, ui) {
                        let dragg = ui.draggable.attr("id").split("Stage_")[1];
                        let dropp = $(this).attr("id").split("Stage_")[1];
                        let correct = false;
                        var _this = this;
                        
                        let {top, left} = $(this).position();
                        limit1--;
                        relation2.forEach(function (item, index) {
                            let droppRuler = item.dropp;
                            let draggRuler = item.dragg;
                            //verrificamos si el dropp esta en los dropps
                            console.log(droppRuler,dropp);
                            if (droppRuler === dropp){
                                //verificamos si el dragg esta en los draggs
                                console.log(ST+dragg);
                                if (draggRuler === dragg) {
                                    correct = true;
                                    points+=increment;
                                    document.querySelector(ST+dragg+" div:nth-of-type(2)").style.display = "block";
                                    ivo.play("good");
                                    ui.draggable.css({
                                        top:top+"px",
                                        left:left+"px",
                                    });
                                    ui.draggable.css({
                                        cursor:"default"
                                    });
                                    //desactimamos draggable
                                    ui.draggable.draggable("disable");
                                    //desactimamos droppable
                                    $(_this).droppable("disable");
                                }else{
                                    ivo.play("bad");
                                    ivo(ST+dropp).hide();
                                    ui.draggable.css({
                                        top:top+"px",
                                        left:left+"px",
                                    });
                                    ui.draggable.css({
                                        cursor:"default"
                                    });
                                    //desactimamos draggable
                                    ui.draggable.draggable("disable");
                                    //desactimamos droppable
                                    $(_this).droppable("disable");
                                }
                            }
                            if (limit2==0){
                                //pasamos al tercer escenario
                                slider.go(5);
                            }
                        });
                    }
                });
            },
            game_dragg_drop3: function () {
                let btns = document.querySelectorAll(".dragg3 div:nth-of-type(2)");
                //los ocultamos
                btns.forEach(function (btn) {
                    btn.style.display = "none";
                });
                $(".dragg3").draggable({
                    revert: "invalid",
                    //centramos el mousel del dragg
                    cursor: "move",
                    cursorAt: {
                        top: 20,
                        left:54
                    },
                    start: function (event, ui) {
                        //ivo.play_audio("clic");
                    }
                });
                $(".dropp3").droppable({
                    accept: ".dragg3",
                    drop: function (event, ui) {
                        let dragg = ui.draggable.attr("id").split("Stage_")[1];
                        let dropp = $(this).attr("id").split("Stage_")[1];
                        let correct = false;
                        var _this = this;
                        
                        let {top, left} = $(this).position();
                        limit3--;
                        relation3.forEach(function (item, index) {
                            let {dropps,draggs} = item;
                            //verrificamos si el dropp esta en los dropps
                            if (dropps.indexOf(dropp) != -1) {
                                //verificamos si el dragg esta en los draggs
                                console.log(ST+dragg);
                                if (draggs.indexOf(dragg) != -1) {
                                    correct = true;
                                    points+=increment;
                                    ivo.play("good");
                                    document.querySelector(ST+dragg+" div:nth-of-type(2)").style.display = "block";
                                    ui.draggable.css({
                                        top:top+"px",
                                        left:left+"px",
                                    });
                                    ui.draggable.css({
                                        cursor:"default"
                                    });
                                    //desactimamos draggable
                                    ui.draggable.draggable("disable");
                                    //desactimamos droppable
                                    $(_this).droppable("disable");
                                }else{
                                    ivo.play("bad");
                                    ivo(ST+dropp).hide();
                                    ui.draggable.css({
                                        top:top+"px",
                                        left:left+"px",
                                    });
                                    ui.draggable.css({
                                        cursor:"default"
                                    });
                                    //desactimamos draggable
                                    ui.draggable.draggable("disable");
                                    //desactimamos droppable
                                    $(_this).droppable("disable");
                                }
                            }
                            if (limit3==0) {
                                //pasamos feedback
                                limit3=-1;
                                slider.go(6);
                                scorm = new MX_SCORM(false);
                                console.log("Estudiante "+scorm.info_user().name+" "+scorm.info_user().id+ " Nota "+points);
                                scorm.set_score(points);


                            }
                        });
                    }
                });
            },
            events: function () {

                var t = this;

                let btns = document.querySelectorAll(".dragg div:nth-of-type(1)");
                //los ocultamos
                btns.forEach(function (btn) {
                    btn.style.display = "none";
                });

                ivo(ST + "stage1").on("click", function () {
                    stage1.timeScale(3).reverse();
                    stage2.timeScale(1).play();
                    ivo.play("clic");
                });
                ivo(ST + "btninicio").on("click", function () {
                    stage2.timeScale(3).reverse();
                    stage1.timeScale(1).play();
                    ivo.play("clic");
                });

                 slider=ivo(ST+"slider").slider({
                    slides:'.slider',
                    btn_next:ST+"btn_sig",
                    btn_back:ST+"btn_atr",
                    rules:function(rule){
                        console.log("rules"+rule);
                    },
                    onMove:function(page){
                        ivo.play("clic");
                        
                    },
                    onFinish:function(){

                    }
                });
                

            },
            animations: function () {
                stage1 = new TimelineMax();
                stage1.append(TweenMax.from(ST + "stage1", .8, {x: 1300, opacity: 0}), 0);
                stage1.append(TweenMax.staggerFrom(ST + "stage1 div", .4, {x: 100, opacity: 0, scaleY: 6, ease: Elastic.easeOut.config(0.3, 0.4)}, .2), 0);
                stage1.stop();
                stage2 = new TimelineMax();
                stage2.append(TweenMax.from(ST + "stage2", .8, {x: 1300, opacity: 0}), 0);
                stage2.append(TweenMax.staggerFrom(".example", .4, {x: 100, opacity: 0, scaleY: 6, ease: Elastic.easeOut.config(0.3, 0.4)}, .2), 0);
                stage2.stop();
            }
        }
    });
}